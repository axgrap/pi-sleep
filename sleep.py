#!/usr/bin/env python
import functools
import os
import threading
import time
from sys import exit
import http.server
from io import BytesIO
import scrollphathd
from scrollphathd.fonts import font5x5
from http.server import BaseHTTPRequestHandler, HTTPServer

override = ""


class HandleRequests(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        root = '.'
        if self.path == '/':
            filename = root + '/index.html'
        else:
            filename = root + self.path
        self.send_response(200)
        if filename[-4:] == '.css':
            self.send_header('Content-type', 'text/css')
        elif filename[-5:] == '.json':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-3:] == '.js':
            self.send_header('Content-type', 'application/javascript')
        elif filename[-4:] == '.ico':
            self.send_header('Content-type', 'image/x-icon')
        else:
            self.send_header('Content-type', 'text/html')
        self.end_headers()
        with open(filename, 'rb') as fh:
            html = fh.read()
            # html = bytes(html, 'utf8')
            self.wfile.write(html)

    def do_POST(self):
        global override
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        self.send_response(200)
        self.end_headers()
        response = BytesIO()
        response.write(b'This is POST request. ')
        response.write(b'Received: ')
        response.write(body)
        print(body.decode('utf-8'))
        if body.decode('utf-8') == "awake":
            override = "awake"
        elif body.decode('utf-8') == "sleep":
            override = "sleep"
        else:
            override = ""
        print(override)
        self.wfile.write(response.getvalue())


PORT = 8080
HOST = ''
Handler = http.server.SimpleHTTPRequestHandler


def start_server():
    print('Starting server...')
    server = HTTPServer((HOST, PORT), HandleRequests)
    thread = threading.Thread(target=server.serve_forever)
    thread.start()


start_server()
# HTTPServer((HOST, PORT), HandleRequests).serve_forever()
print("server started")
try:
    from PIL import Image
except ImportError:
    exit("This script requires the pillow module\nInstall with: sudo pip install pillow")

print("""
GO TO SLEEP OBIE!
I NEED  SOME REST!

crtl-c to quit...
""")

IMAGE_BRIGHTNESS = 0.2

happy = Image.open("happy.bmp")
sleep1 = Image.open("sleep1.bmp")
sleep2 = Image.open("sleep2.bmp")


def get_happy_pixel(x, y):
    p = happy.getpixel((x, y))

    if happy.getpalette() is not None:
        r, g, b = happy.getpalette()[p:p + 3]
        p = max(r, g, b)

    return p / 255.0


def get_sleep1_pixel(x, y):
    p = sleep1.getpixel((x, y))

    if sleep1.getpalette() is not None:
        r, g, b = sleep1.getpalette()[p:p + 3]
        p = max(r, g, b)

    return p / 255.0


def get_sleep2_pixel(x, y):
    p = sleep2.getpixel((x, y))

    if sleep2.getpalette() is not None:
        r, g, b = sleep2.getpalette()[p:p + 3]
        p = max(r, g, b)

    return p / 255.0


currentMood = ""

try:
    while True:
        previousMood = currentMood
        if (
                time.localtime().tm_hour >= 7 and time.localtime().tm_hour <= 19 and override != "sleep") or override == "awake":
            currentMood = "happy"
            for x in range(0, scrollphathd.DISPLAY_HEIGHT):
                for y in range(0, scrollphathd.DISPLAY_WIDTH):
                    brightness = get_happy_pixel(x, y)
                    scrollphathd.set_pixel(x, y, brightness * IMAGE_BRIGHTNESS)
        else:
            if int(time.time()) % 2 == 0:
                currentMood = "sleep"
                for x in range(0, scrollphathd.DISPLAY_HEIGHT):
                    for y in range(0, scrollphathd.DISPLAY_WIDTH):
                        brightness = get_sleep1_pixel(x, y)
                        scrollphathd.set_pixel(x, y, brightness * IMAGE_BRIGHTNESS)
            else:
                currentMood = "sleep"
                for x in range(0, scrollphathd.DISPLAY_HEIGHT):
                    for y in range(0, scrollphathd.DISPLAY_WIDTH):
                        brightness = get_sleep2_pixel(x, y)
                        scrollphathd.set_pixel(x, y, brightness * IMAGE_BRIGHTNESS)

        # Display the time (HH) in a 5x5 pixel font
        scrollphathd.rotate(270)
        scrollphathd.write_string(
            time.strftime("%H"),
            x=0,
            y=0,
            font=font5x5,
            brightness=IMAGE_BRIGHTNESS
        )
        scrollphathd.show()
        if previousMood != currentMood:
            print(currentMood)

except KeyboardInterrupt:
    scrollphathd.clear()
    scrollphathd.show()
